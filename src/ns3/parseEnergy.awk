#!/usr/bin/awk -f

BEGIN {
    durationECOFEN=0
    sum=0
}

# For ECOFEN energy model
/Node [0-9]+ Conso/ {
    if (!($4 in energyECOFEN)){
        energyECOFEN[$4]=$6
        countECOFEN[$4]=1
    }
    else {
        energyECOFEN[$4]=$6+energyECOFEN[$4]
        countECOFEN[$4]++
    }
    durationECOFEN=$2
}

# For WIFI ns-3 energy model
/Node -?[0-9]+ consumes/ {
    gsub("J","",$4) # Remove trailling Joule symbol
    energyWIFI[$2]=$4
}



END {
    # Extract ECOFEN energy
    for(key in energyECOFEN){
        if (countECOFEN[key]>0){ # Otherwise: 0 division
            overallEnergy[key]=energyECOFEN[key]/countECOFEN[key]*durationECOFEN
        }
        else { 
            overallEnergy[key]=0
        }
    }

    # Extract WIFI energy
    for(key in energyWIFI){
        if(key in overallEnergy){ # Combine WIFI+ECOFEN
            overallEnergy[key]+=energyWIFI[key] # Add wifi to ECOFEN
        }
        else {
            overallEnergy[key]=energyWIFI[key] # Only add WIFI since there is no ECOFEN value
        }
    }

    # CSV output
    print("nodeId,energy")
    for(key in overallEnergy){
        print(key "," overallEnergy[key])
    }
}
