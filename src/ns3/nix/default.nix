{
  pkgs ? (import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {})
}:

with pkgs; rec {

     ns3 = stdenv.mkDerivation rec {
        ##### Configure NIX #####
        name="ns3";
        sourceRoot="ns-allinone-3.29/ns-3.29/"; # Since we have 2 source tarball (ns-3 & ECOFEN) nix need to know which one to use

        ##### Fetch ns-3 And ECOFEN #####
        src = [
            (fetchurl {
            url = https://www.nsnam.org/releases/ns-allinone-3.29.tar.bz2;
            sha256 = "0m9dpmby116qk1m4x645i1p92syn30yzn9dgxxji5i25g30abpsd";
            })

            (fetchurl {
            url = http://people.irisa.fr/Anne-Cecile.Orgerie/ECOFEN/ecofen-v2.tar.bz2;
            sha256 = "1dnmm20ihas6hwwb8qbx8sr3h66nrg8h55x6f2aqpf3xima29dyh";
            })
        ];

        ##### Configure Dependencies #####
        buildInputs= [ python gsl ];

        ##### Configure Phases #####
        postUnpack=''mv ecofen-module-v2 ${sourceRoot}/contrib/ecofen'';
        configurePhase=''
        export CXXFLAGS="-Wall -g -O0" # Don't treat warning as error when compiling ns-3
        python2 waf configure
        '';
        buildPhase=''python2 waf'';
        installPhase=''
        mkdir -p $out/include
        cp -r ./build/lib $out/
        cp -r ./build/ns3 $out/include
        '';
    };
    
    simulator= stdenv.mkDerivation rec {
        ##### Configure NIX #####
        name="simulator";
        src=./simulator;

        #####  Export ns3 location #####
        NS3_PATH=ns3;

        ##### Configure Phases #####
        buildPhase=''make'';
        installPhase=''
        mkdir -p $out/bin
        install -D -t $out/bin simulator
        '';

      };
}
