#include "modules/modules.hpp"
#ifndef NS3_VERSION
#define NS3_VERSION "unknown"
#endif

NS_LOG_COMPONENT_DEFINE ("WIFISensorsSimulator");


/**
 * To get more details about functions please have a look at modules/modules.hpp
 */
int main(int argc, char* argv[]){

  uint32_t sensorsFrequency=10; // 1 pkt every 10 seconds
  uint32_t sensorsPktSize=192; // 128 bits for sensors id and 32 bit for the temperature (an arbitrary Integer) and a timestamp (32bits)
  uint32_t sensorsNumber=5;
  uint32_t nbHop=10;
  uint32_t linksBandwidth=10000;
  uint32_t linksLatency=11; // 10 hops =>  9 links => 11.1ms of latency => end-to-end latency==100ms
  uint32_t positionSeed=5; // arbitrary

  CommandLine cmd;
  cmd.AddValue ("sensorsSendInterval", "Number of sensors measurement per second", sensorsFrequency);
  cmd.AddValue ("sensorsPktSize", "Sensors packet size (bytes)", sensorsPktSize);
  cmd.AddValue ("sensorsNumber", "Number of sensors connected to AP", sensorsNumber);
  cmd.AddValue ("nbHop", "Number of hop between AP and Cloud", nbHop);
  cmd.AddValue ("linksBandwidth", "Links bandwidth between AP and Cloud", linksBandwidth);
  cmd.AddValue ("linksLatency", "Links latency between AP and Cloud", linksLatency);
  cmd.AddValue ("positionSeed", "RandomRectangle Sensors placement seed", positionSeed);
  cmd.Parse (argc, argv);

  // Check sensors frequency
  if(sensorsFrequency<1){
    NS_LOG_UNCOND("SensorsSendInterval too small: " << sensorsFrequency << " (it should be >1)." );
    exit(1);
  }
  
  //LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
  //LogComponentEnable("PacketSink", LOG_LEVEL_INFO);

  // ---------- Setup Simulations ----------
  CloudInfos cloud=createCloud(nbHop,linksBandwidth,linksLatency); // Create cloud P2P node chain o--o--o--o--o
  setupCloudEnergy(cloud); // DO IT JUST AFTER createCloud !!!!! Otherwise you will be in trouble
  Cell cell=createCell(sensorsNumber,cloud.first.Get(0),positionSeed); // Use first cloud node as Access Point
  setupScenario(cell,cloud,sensorsPktSize,sensorsFrequency); // Send data from Sensors to Cloud
  DeviceEnergyModelContainer wifi=setupCellEnergy(cell);
  
  // Don't forget the following
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  
  // Setup Logs
  uint32_t nNode=ns3::NodeList::GetNNodes();
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  
  // Run Simulations  
  Simulator::Stop (Seconds (SIM_TIME));
  Simulator::Run ();

  // Print logs
  NS_LOG_UNCOND("NS-3 Version " << NS3_VERSION);
  NS_LOG_UNCOND("Simulation used "<< nNode << " nodes");
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map< FlowId, FlowMonitor::FlowStats>::iterator flow=stats.begin(); flow!=stats.end(); flow++)
    {
      Ipv4FlowClassifier::FiveTuple  t = classifier->FindFlow(flow->first);
      //NS_LOG_UNCOND("Flow " <<t.sourceAddress<<  " -> "<< t.destinationAddress << " delay = " <<flow->second.delaySum.GetSeconds());
      NS_LOG_UNCOND("Flow " <<t.sourceAddress<<  " -> "<< t.destinationAddress << " delay = " <<flow->second.delaySum.GetSeconds()/flow->second.rxPackets); 
    }


  // Trace
  DeviceEnergyModelContainer::Iterator it=wifi.Begin();
  int i=0; // Note that node 0 is the AP
  while(it!=wifi.End()){
    NS_LOG_UNCOND ("Node " << i << " consumes " <<(*it)->GetTotalEnergyConsumption());
    it++;
    i--; // Edge device will have id < 0 and ap will have id 0
  }

  
  // Finish
  Simulator::Destroy ();  
  return(0);
}
