
#include "modules.hpp"

DeviceEnergyModelContainer setupCellEnergy(Cell cell){
  NodeContainer nodes(cell.first.first,cell.first.second);
  NetDeviceContainer nodesNetDev(cell.second.first,cell.second.second);

  // Install energy source
  BasicEnergySourceHelper edgeBasicSourceHelper;
  edgeBasicSourceHelper.Set ("BasicEnergySourceInitialEnergyJ", DoubleValue (BASICENERGYSOURCEINITIALENERGYJ));
  edgeBasicSourceHelper.Set ("BasicEnergySupplyVoltageV", DoubleValue (BASICENERGYSUPPLYVOLTAGEV));
  EnergySourceContainer apEdgeNodesSources = edgeBasicSourceHelper.Install (cell.first.first);
  EnergySourceContainer wifiEdgeNodesSources = edgeBasicSourceHelper.Install (cell.first.second);

  // Install device energy model
  WifiRadioEnergyModelHelper radioEnergyHelper;
  radioEnergyHelper.Set ("TxCurrentA", DoubleValue (TXCURRENTA));
  radioEnergyHelper.Set ("RxCurrentA", DoubleValue (RXCURRENTA));
  radioEnergyHelper.Set ("IdleCurrentA", DoubleValue (IDLECURRENTA));
  DeviceEnergyModelContainer edgeApDeviceModels = radioEnergyHelper.Install (cell.second.first, apEdgeNodesSources);
  DeviceEnergyModelContainer edgeDeviceModels = radioEnergyHelper.Install (cell.second.second, wifiEdgeNodesSources);

  
  // Trace
  //  DeviceEnergyModelContainer::Iterator it=edgeDeviceModels.Begin();
  //int i=1; // Node 0 will be AP, other node will have negative id (cf following while)
           // This is usefull in logs, in fact ECOFEN nodes will have positive ID and WIFI energy nodes negative id
           // AP will have id 0 in ECOFEN and WIFI (in order to combine their energy value when parsing logs
  // while(it!=edgeDeviceModels.End()){
  //   (*it)->TraceConnect ("TotalEnergyConsumption", std::to_string(0-i),MakeCallback (&EnergyUpdated));
  //   it++;
  //   i++;
  // }
  // // AP will have id 0
  // (*edgeApDeviceModels.Begin())->TraceConnect ("TotalEnergyConsumption", std::to_string(0),MakeCallback (&EnergyUpdated));

 //   Ptr<BasicEnergySource> basicSourcePtr0 = DynamicCast<BasicEnergySource> (wifiEdgeNodesSources.Get (0));
  // //basicSourcePtr0->TraceConnectWithoutContext ("RemainingEnergy", MakeCallback (&RemainingEnergy));
  // //device energy model
  // Ptr<DeviceEnergyModel> basicRadioModelPtr0 =
  // basicSourcePtr0->FindDeviceEnergyModels ("ns3::WifiRadioEnergyModel").Get (0);
  // NS_ASSERT (basicRadioModelPtr0 != NULL);
  // basicRadioModelPtr0->TraceConnectWithoutContext ("TotalEnergyConsumption", MakeCallback (&TotalEnergy));
  return(DeviceEnergyModelContainer(edgeApDeviceModels,edgeDeviceModels));
}

void setupCloudEnergy(CloudInfos cloudInfos){
  NodeContainer cloudNodes=cloudInfos.first;
  
  // Install basic energy
  ns3::BasicNodeEnergyHelper basicNodeEnergy;
  basicNodeEnergy.Set("OnConso", ns3::DoubleValue (ONCONSO));
  basicNodeEnergy.Set("OffConso", ns3::DoubleValue (OFFCONSO));
  basicNodeEnergy.Install (cloudNodes);

  ns3::CompleteNetdeviceEnergyHelper completeNetdeviceEnergy;
  completeNetdeviceEnergy.Set ("OffConso", ns3::DoubleValue (OFFCONSO));
  completeNetdeviceEnergy.Set ("IdleConso", ns3::DoubleValue (IDLECONSO));
  completeNetdeviceEnergy.Set ("RecvByteEnergy", ns3::DoubleValue (RECVBYTEENERGY));
  completeNetdeviceEnergy.Set ("SentByteEnergy", ns3::DoubleValue (SENTBYTEENERGY));
  completeNetdeviceEnergy.Set ("RecvPktEnergy", ns3::DoubleValue (RECVPKTENERGY));
  completeNetdeviceEnergy.Set ("SentPktEnergy", ns3::DoubleValue (SENTPKTENERGY));
  completeNetdeviceEnergy.Install(cloudNodes);
  
  ns3::ConsumptionLogger conso;
  conso.NodeConso(ns3::Seconds (ECOFEN_LOG_EVERY), ns3::Seconds(SIM_TIME), cloudNodes);
}

