
#ifndef MODULES_HPP
#define MODULES_HPP

#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/udp-echo-helper.h"
#include "ns3/tcp-westwood.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/energy-module.h"
#include "ns3/wifi-radio-energy-model-helper.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/ecofen-module.h"
#include "ns3/node-list.h"
#include "ns3/flow-monitor-module.h"

// C++ library
#include <iostream> // Why not ?
#include <utility>  // To use std::pair
#include <iomanip>  // To use std::setw

#define SIM_TIME 1800 // 30mins simulations
#define RECT_SIZE 20 // Sensors random rectangle position size
#define MAX_PACKET_BY_SENSOR 900000 // Reasonable big number (in order that simulation end before sensors stop sending packets)

// ECOFEN
#define ECOFEN_LOG_EVERY 0.5

// WIFI Energy Values
#define BASICENERGYSOURCEINITIALENERGYJ 10000000
#define BASICENERGYSUPPLYVOLTAGEV 3.3
#define TXCURRENTA 0.38
#define RXCURRENTA 0.313
#define IDLECURRENTA 0.273

// Cloud Energy Values
#define ONCONSO 0
#define OFFCONSO 0
#define IDLECONSO 0.53
#define RECVBYTEENERGY 14
#define SENTBYTEENERGY 14
#define RECVPKTENERGY 1504
#define SENTPKTENERGY 1504

using namespace ns3;

// ---------- Data types ----------
typedef std::pair<NodeContainer,NodeContainer> CellNodes; // Format (APNode, SensorsNodes)
typedef std::pair<NetDeviceContainer,NetDeviceContainer> CellNetDevices; // Format (APNetDev, SensorsNetDev)
typedef std::pair<CellNodes,CellNetDevices> Cell;
typedef std::pair<Ipv4Address,int> EndPoint; // Format (IP,Port)
typedef std::pair<NodeContainer,EndPoint> CloudInfos; // Format (CloudHops,CloudEndPoint), here data sent to CloudEndPoint


// ---------- platform.cc ----------
/**
 * Create a WIFI cell paltform composed of nbSensors sensors and ap as an access point
 */
Cell createCell(uint32_t nbSensors, Ptr<ns3::Node> ap,int positionSeed);

/**
 * Build P2P network composed of nbHop hops (to simulate edge->cloud communications)
 * Note: Cloud Servers are not considered here and completely ignored !
 */
CloudInfos createCloud(int nbHop, uint32_t bandwidth, uint32_t latency);
/**
 * Setup simulation scenario on the platforms. Sensors in cell will send packets of sensorsPktSize size every
 * sensorsSensInterval second to the cloud using cloudInfos.
 */
void setupScenario(Cell cell, CloudInfos cloudInfos, int sensorsPktSize, int sensorsSendInterval);


// ---------- energy.cc ----------
/*
 * Configure WIFI energy module for cell
 */
DeviceEnergyModelContainer setupCellEnergy(Cell cell);
/*
 * Configure link/port energy using ecofen
 */
void setupCloudEnergy(CloudInfos cloudInfos);


// ---------- callbacks.cc ----------
void PktReceived(std::string nodeName,Ptr< const Packet > packet, const Address &address);
void EnergyUpdated(std::string nodeName,double oldValue, double newValue);



#endif
