
#include "modules.hpp"

void PktReceived(std::string nodeName,Ptr< const Packet > packet, const Address &address){
  NS_LOG_UNCOND("Node " << nodeName << " receive a packet" << " at time " << Simulator::Now ().GetSeconds () << "s");
}

void EnergyUpdated(std::string nodeName,double oldValue, double newValue){
  double currentTime=Simulator::Now ().GetSeconds ();
  double energyConsumes=newValue-oldValue;
  NS_LOG_UNCOND("Node " << nodeName << " consumes " <<  energyConsumes << "J" << " at time " << currentTime  << "s");
}
