#!/bin/bash
##### Arguments #####
nHost=20 # At least 20 host x)
nProcesses=3 # Max number of parrallel simulations (don't go too high, your process will be killed (arround 8))
nHours=4 # Reservation duration
simArgsLoc=~/args/ # Don't change this path witouth changing it in workder scripts
finishedFile="$simArgsLoc/finished-microBenchmarks.txt"
logsFinalDst=~/logs/
#####################

# Check
[ "$1" == "subscribe" ] && subscribe=1 ||subscribe=0
[ "$1" == "deploy" ] && deploy=1 || deploy=0
[ "$1" == "-p" ] && progress=1 || progress=0

handleSim () {
    [ -z "${argId}" ] && argId=1 || argId=$(( argId + 1 ))
    outF="$simArgsLoc/${argId}.sh" # Args file based on host name (avoid conflict)

    # Add Shebang
    echo '#!/bin/bash' > $outF
    echo "finishedFile=\"$finishedFile\"" >> $outF
    echo "nProcesses=$nProcesses" >> $outF
    echo "logsFinalDst=\"$logsFinalDst\"" >> $outF
    # Save arguments
    echo "sensorsSendInterval=${sensorsSendInterval}" >> $outF
    echo "sensorsPktSize=${sensorsPktSize}" >> $outF
    echo "nbHop=${nbHop}" >> $outF
    echo "simKey=\"${simKey}\"" >> $outF
    echo "sensorsNumber=${sensorsNumber}" >> $outF
    echo "linksLatency=${linksLatency}" >> $outF
    echo "sensorsNumber=${sensorsNumber}" >> $outF
    echo "linksBandwidth=${linksBandwidth}" >> $outF
    echo "positionSeed=${positionSeed}" >> $outF
}


# Start subscribe/deploy
if [ $subscribe -eq 1 ]
then
    echo "Starting oarsub..."
    oarsub -l host=$nHost,walltime=$nHours 'sleep "10d"' # Start reservation
    echo "Please join your node manually when your reservation is ready by using oarsub -C <job-id>"
    exit 0
elif [ $deploy -eq 1 ]
then
    echo "Starting deployment..."

    ##### Usefull Variables #####
    wai=$(dirname "$(readlink -f $0)") # Where Am I ?
    hostList=($(cat $OAR_NODE_FILE | uniq))
    #############################

    # Initialize logsFinalDst
    mkdir -p $logsFinalDst
    rm -rf $logsFinalDst/* # Clean log dst just in case (it is dangerous but avoid conflict)
    mkdir -p $simArgsLoc
    rm -rf $simArgsLoc/* # Clean old args

    # Add your simulation code block here
    simulator="simulator/simulator"
    parseEnergyScript="./parseEnergy.awk"
    parseDelayScript="./parseDelay.awk"
    logFolder="logs/"
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${NS3_PATH}/build/lib
    
    # Default Parameters
    sensorsSendInterval=10 # DON'T GO BELOW 1 SECONDS !!!!!!! Simulator will stay stuck
    sensorsPktSize=192 # 1 byte temperature (-128 à +128 °C) and 4Byte sensorsId
    sensorsNumber=5
    nbHop=10 # Cf paper AC/Yunbo
    linksBandwidth=10000 # 10Ge links (to be coherent with energy values of ECOFEN/literature)
    linksLatency=11
    positionSeed=5
    simKey="NOKEY"
    
    run () {
        # If another function want to handle simulation (tipically used on g5k)
        type -t handleSim > /dev/null && { handleSim; return; }
    
        local logFile="${logFolder}/${simKey}_${sensorsSendInterval}SSI_${sensorsPktSize}SPS_${sensorsNumber}SN_${nbHop}NH_${linksBandwidth}LB_${linksLatency}LL_${positionSeed}PS.org"
        [ -f "$logFile" ] && return
        local simCMD="$simulator --sensorsSendInterval=${sensorsSendInterval} --sensorsPktSize=${sensorsPktSize} --sensorsNumber=${sensorsNumber} --nbHop=${nbHop} --linksBandwidth=${linksBandwidth} --linksLatency=${linksLatency} --positionSeed=${positionSeed} 2>&1"
        local log=$(bash -c "$simCMD")
    
        # Compute some metrics
        energyLog=$(echo "$log" | $parseEnergyScript)
        avgDelay=$(echo "$log" | $parseDelayScript)
        totalEnergy=$(echo "$energyLog" | awk 'BEGIN{power=0;FS=","}NR!=1{power+=$2}END{print(power)}')
        sensorsEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumW=0}$1<0{sumW+=$2}END{print sumW}')
        networkEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumN=0}$1>=0{sumN+=$2}END{print sumN}')
        nbPacketCloud=$(echo "$log"|grep -c "CloudSwitch receive")
        nbNodes=$(echo "$log"|awk '/Simulation used/{print($3)}')
        ns3Version=$(echo "$log"|awk '/NS-3 Version/{print($3)}')
    
        # Save logs
        echo -e "#+TITLE: $(date) ns-3 (version ${ns3Version}) simulation\n" > $logFile
        echo "* Environment Variables" >> $logFile
        env >> $logFile
        echo "* Full Command" >> $logFile
        echo "$simCMD" >> $logFile
        echo "* Output" >> $logFile
        echo "$log" >> $logFile
        echo "* Energy CSV (negative nodeId = WIFI, 0 = AP (Wireless+Wired), positive nodeId = ECOFEN" >> $logFile
        echo "$energyLog" >> $logFile
        echo "* Metrics" >> $logFile
        echo "-METRICSLINE- sensorsSendInterval:${sensorsSendInterval} sensorsPktSize:${sensorsPktSize} sensorsNumber:${sensorsNumber} nbHop:${nbHop} linksBandwidth:${linksBandwidth} linksLatency:${linksLatency} totalEnergy:$totalEnergy nbPacketCloud:$nbPacketCloud nbNodes:$nbNodes avgDelay:${avgDelay} ns3Version:${ns3Version} simKey:${simKey} positionSeed:${positionSeed} sensorsEnergy:${sensorsEnergy} networkEnergy:${networkEnergy}" >> $logFile
    }
    
    simKey="NBSENSORS"
    for sensorsNumber in $(seq 1 15)
    do
        run
    done      
    simulator="simulator/simulator"
    parseEnergyScript="./parseEnergy.awk"
    parseDelayScript="./parseDelay.awk"
    logFolder="logs/"
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${NS3_PATH}/build/lib
    
    # Default Parameters
    sensorsSendInterval=10 # DON'T GO BELOW 1 SECONDS !!!!!!! Simulator will stay stuck
    sensorsPktSize=192 # 1 byte temperature (-128 à +128 °C) and 4Byte sensorsId
    sensorsNumber=5
    nbHop=10 # Cf paper AC/Yunbo
    linksBandwidth=10000 # 10Ge links (to be coherent with energy values of ECOFEN/literature)
    linksLatency=11
    positionSeed=5
    simKey="NOKEY"
    
    run () {
        # If another function want to handle simulation (tipically used on g5k)
        type -t handleSim > /dev/null && { handleSim; return; }
    
        local logFile="${logFolder}/${simKey}_${sensorsSendInterval}SSI_${sensorsPktSize}SPS_${sensorsNumber}SN_${nbHop}NH_${linksBandwidth}LB_${linksLatency}LL_${positionSeed}PS.org"
        [ -f "$logFile" ] && return
        local simCMD="$simulator --sensorsSendInterval=${sensorsSendInterval} --sensorsPktSize=${sensorsPktSize} --sensorsNumber=${sensorsNumber} --nbHop=${nbHop} --linksBandwidth=${linksBandwidth} --linksLatency=${linksLatency} --positionSeed=${positionSeed} 2>&1"
        local log=$(bash -c "$simCMD")
    
        # Compute some metrics
        energyLog=$(echo "$log" | $parseEnergyScript)
        avgDelay=$(echo "$log" | $parseDelayScript)
        totalEnergy=$(echo "$energyLog" | awk 'BEGIN{power=0;FS=","}NR!=1{power+=$2}END{print(power)}')
        sensorsEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumW=0}$1<0{sumW+=$2}END{print sumW}')
        networkEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumN=0}$1>=0{sumN+=$2}END{print sumN}')
        nbPacketCloud=$(echo "$log"|grep -c "CloudSwitch receive")
        nbNodes=$(echo "$log"|awk '/Simulation used/{print($3)}')
        ns3Version=$(echo "$log"|awk '/NS-3 Version/{print($3)}')
    
        # Save logs
        echo -e "#+TITLE: $(date) ns-3 (version ${ns3Version}) simulation\n" > $logFile
        echo "* Environment Variables" >> $logFile
        env >> $logFile
        echo "* Full Command" >> $logFile
        echo "$simCMD" >> $logFile
        echo "* Output" >> $logFile
        echo "$log" >> $logFile
        echo "* Energy CSV (negative nodeId = WIFI, 0 = AP (Wireless+Wired), positive nodeId = ECOFEN" >> $logFile
        echo "$energyLog" >> $logFile
        echo "* Metrics" >> $logFile
        echo "-METRICSLINE- sensorsSendInterval:${sensorsSendInterval} sensorsPktSize:${sensorsPktSize} sensorsNumber:${sensorsNumber} nbHop:${nbHop} linksBandwidth:${linksBandwidth} linksLatency:${linksLatency} totalEnergy:$totalEnergy nbPacketCloud:$nbPacketCloud nbNodes:$nbNodes avgDelay:${avgDelay} ns3Version:${ns3Version} simKey:${simKey} positionSeed:${positionSeed} sensorsEnergy:${sensorsEnergy} networkEnergy:${networkEnergy}" >> $logFile
    }
    
    simKey="SENDINTERVAL"
    for sensorsNumber in $(seq 5 2 15)
    do
        for sensorsSendInterval in $(seq 10 10 100)
        do
            run
        done      
    done
    simulator="simulator/simulator"
    parseEnergyScript="./parseEnergy.awk"
    parseDelayScript="./parseDelay.awk"
    logFolder="logs/"
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${NS3_PATH}/build/lib
    
    # Default Parameters
    sensorsSendInterval=10 # DON'T GO BELOW 1 SECONDS !!!!!!! Simulator will stay stuck
    sensorsPktSize=192 # 1 byte temperature (-128 à +128 °C) and 4Byte sensorsId
    sensorsNumber=5
    nbHop=10 # Cf paper AC/Yunbo
    linksBandwidth=10000 # 10Ge links (to be coherent with energy values of ECOFEN/literature)
    linksLatency=11
    positionSeed=5
    simKey="NOKEY"
    
    run () {
        # If another function want to handle simulation (tipically used on g5k)
        type -t handleSim > /dev/null && { handleSim; return; }
    
        local logFile="${logFolder}/${simKey}_${sensorsSendInterval}SSI_${sensorsPktSize}SPS_${sensorsNumber}SN_${nbHop}NH_${linksBandwidth}LB_${linksLatency}LL_${positionSeed}PS.org"
        [ -f "$logFile" ] && return
        local simCMD="$simulator --sensorsSendInterval=${sensorsSendInterval} --sensorsPktSize=${sensorsPktSize} --sensorsNumber=${sensorsNumber} --nbHop=${nbHop} --linksBandwidth=${linksBandwidth} --linksLatency=${linksLatency} --positionSeed=${positionSeed} 2>&1"
        local log=$(bash -c "$simCMD")
    
        # Compute some metrics
        energyLog=$(echo "$log" | $parseEnergyScript)
        avgDelay=$(echo "$log" | $parseDelayScript)
        totalEnergy=$(echo "$energyLog" | awk 'BEGIN{power=0;FS=","}NR!=1{power+=$2}END{print(power)}')
        sensorsEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumW=0}$1<0{sumW+=$2}END{print sumW}')
        networkEnergy=$(echo "$energyLog" |awk -F',' 'BEGIN{sumN=0}$1>=0{sumN+=$2}END{print sumN}')
        nbPacketCloud=$(echo "$log"|grep -c "CloudSwitch receive")
        nbNodes=$(echo "$log"|awk '/Simulation used/{print($3)}')
        ns3Version=$(echo "$log"|awk '/NS-3 Version/{print($3)}')
    
        # Save logs
        echo -e "#+TITLE: $(date) ns-3 (version ${ns3Version}) simulation\n" > $logFile
        echo "* Environment Variables" >> $logFile
        env >> $logFile
        echo "* Full Command" >> $logFile
        echo "$simCMD" >> $logFile
        echo "* Output" >> $logFile
        echo "$log" >> $logFile
        echo "* Energy CSV (negative nodeId = WIFI, 0 = AP (Wireless+Wired), positive nodeId = ECOFEN" >> $logFile
        echo "$energyLog" >> $logFile
        echo "* Metrics" >> $logFile
        echo "-METRICSLINE- sensorsSendInterval:${sensorsSendInterval} sensorsPktSize:${sensorsPktSize} sensorsNumber:${sensorsNumber} nbHop:${nbHop} linksBandwidth:${linksBandwidth} linksLatency:${linksLatency} totalEnergy:$totalEnergy nbPacketCloud:$nbPacketCloud nbNodes:$nbNodes avgDelay:${avgDelay} ns3Version:${ns3Version} simKey:${simKey} positionSeed:${positionSeed} sensorsEnergy:${sensorsEnergy} networkEnergy:${networkEnergy}" >> $logFile
    }
    
    simKey="SENSORSPOS"
    for sensorsNumber in $(seq 5 2 15)
    do
        for positionSeed in $(seq 1 10)
        do
            run
        done      
    done

    # Distribute argument according to subsribed nodes
    cd $simArgsLoc
    curHostId=0
    for file in $(find ./ -type f)
    do
        [ $curHostId -eq $nHost ] && curHostId=0  
        mv -- ${file} ${hostList[$curHostId]}-$(basename ${file})
        curHostId=$(( curHostId + 1 ))
    done
    cd -


    # Run simulations
    echo "Host who finished their work:" > $finishedFile
    for host in ${hostList[@]}
    do
        echo "Start simulations on node $host"
        oarsh lguegan@$host bash g5k-worker.sh &
    done

    exit 0
elif [ $progress -eq 1 ]
then
    alreadyFinished=$(cat $finishedFile| tail -n +2| wc -l)
    percent=$(echo $alreadyFinished $nHost| awk '{print $1/$2*100}')
    echo "Progression: " $alreadyFinished/$nHost "(${percent}%)"
else
    echo "Invalid arguments, make sure you know what you are doing !"
    exit 1
fi
