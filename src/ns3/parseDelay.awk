#!/usr/bin/awk -f

BEGIN {
    delaySum=0
    delayCount=0
}

/delay =/ {
    gsub("ns","",$7)
    gsub("+","",$7)
    delaySum+=$7
    delayCount+=1
}

END {
    if(delayCount>0)
        print(delaySum/delayCount)
    else
        print(0)
}
