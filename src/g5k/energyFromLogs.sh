#!/bin/bash


logFile="$(dirname $(readlink -f $0))"/simLogs.txt


getValue () {
    line=$(echo "$1" | grep "Simulation para"|sed "s/Simulation parameters: //g")
    key=$2
    echo "$line"|awk 'BEGIN{RS=" ";FS=":"}"'$key'"==$1{gsub("\n","",$0);print $2}'
}

IFS=$'\n'
for cmd in $(cat $logFile|grep "Simulation parameters")
do
    nodeName=$(getValue $cmd serverNodeName)
    from=$(getValue $cmd simStart)
    to=$(getValue $cmd simEnd)
    vmSize=$(getValue $cmd vmSize)
    nbSensors=$(getValue $cmd nbSensors)
    simKey=$(getValue $cmd simKey)
    delayStart=$(getValue $cmd delayStart)
    python ./wattmeters.py $nodeName $from $to
    mv power.csv "${simKey}_${vmSize}VMSIZE_${nbSensors}NBSENSORS_${from}${to}.csv"
    python ./wattmeters.py $nodeName $delayStart $from
    mv power.csv "${simKey}_${vmSize}VMSIZE_${nbSensors}NBSENSORS_${from}${to}_IDLE.csv"
done

