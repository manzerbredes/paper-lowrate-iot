#!/bin/bash

##### Parameters #####
if [ -z ${nbSensors+x} ] # If nbSensors exists, so all parameters are already define
then
    delay=60 # Delay before starting simulation (let CPU energy going down on the server)
    nbSensors=30 # Number of sensors that will send request to de server
    nbSensorsFactor=3 # nbSensors*nbSensorFactor
    simulationTime=300 # Approximative
    sensorsSendInterval=10 # Delay between sensors requests
    vmSize=2048 # Number of alocated ram
    simKey="NONE"
fi
nHours=3 # Reservation dutation
requestPerSensor=$(( simulationTime / sensorsSendInterval )) # Theorical simulation time is $requestPerSensor*$sensorsSendInterval
######################


logFile="./simLogs.txt"
log () {
    echo -e "\033[0;34m$@\033[0m"
}

sshWait () {
     log "Waiting for for an ssh connection to a vm ($1)"
     error=255
     until [ $error -eq 0 ]
     do
         ssh -q root@$1 echo "Connected to $(hostname)"
         error=$?
         sleep 4
     done
}

if [ "$1" = "subscribe" ] && [ $# -eq 1 ]
then
    log "Subscribing..."
    oarsub -l slash_22=1+{"virtual!='NO' AND cluster='nova'"}/nodes=2,walltime=$nHours 'sleep "10d"' # On node send request to the other
#    oarsub -l slash_22=1+{"virtual!='NO'"}/nodes=2,walltime=$nHours 'sleep "10d"' # On node send request to the other

elif [ "$1" = "deploy" ] && [ $# -eq 1 ]
then
     # Get machine mac address
     serverSubnet=$(g5k-subnets -im|sed "1q;d")
     serverMac=$(echo "$serverSubnet"|sed "s/^.*\t//g")
     serverIp=$(echo "$serverSubnet"|sed "s/\t.*$//g")
     clientIp=$(hostname) # Not really a IP but :P
     clientNode=$(hostname)
     serverNode=$(cat $OAR_NODE_FILE|uniq|sed "s/$clientNode//g"|sed "s/ //g"|tr -d '\n')
     onS="oarsh $serverNode" # For convenience


     # Init vm images
     log "Create server vm image"
     $onS cp -n /grid5000/virt-images/debian9-x64-base.qcow2 /tmp/
     $onS qemu-img create -f qcow2 -o backing_file=/tmp/debian9-x64-base.qcow2 /tmp/img.qcow2
     sleep 1 # Wait for fun

     # Build cloud init iso (to have ssh access witouth password
     log "Create server cloud-init image"
     $onS cp /grid5000/virt-images/cloud-init-example.sh /tmp/
     $onS "cd /tmp && export cloud_init_key=\$(cat ~/.ssh/id_rsa.pub) && ./cloud-init-example.sh"
     $onS "cd /tmp && genisoimage -output cloud-init-data.iso -volid cidata -joliet -rock cloud-init-data/user-data cloud-init-data/meta-data"

     # Launch vm
     log "Launch server vm"
     $onS kvm -m ${vmSize}M -hda /tmp/img.qcow2 -netdev bridge,id=br0 -device virtio-net-pci,netdev=br0,id=nic1,mac=$serverMac -cdrom /tmp/cloud-init-data.iso -display none -daemonize &

     
     ##### Server #####
     onS="ssh root@$serverIp" # Don't forget to use vm
     sshWait $serverIp
     # One apt-get update seems to be not enought to get mysql-server
     $onS "apt-get update && apt-get update"
     $onS apt-get -y install mysql-server
     # Enable mysql connection from outside
     $onS sed -i "s/bind-address/#bind-address/g" /etc/mysql/mariadb.conf.d/50-server.cnf
     $onS 'echo -e "[mysqld]\nmax_connections = 100000" >> /etc/mysql/my.cnf' # Otherwise you will have the error "TOO MANY CONNECTION"
     $onS systemctl restart mysql
     rsync -avh setup-mysql.sql root@$serverIp:/tmp/ # Send mysl setup script
     $onS "mysql < /tmp/setup-mysql.sql"      # Then execute it

     ##### Start Simulation #####
     serverNodeName=$(echo $serverNode|grep -o ^.*[-][0-9]*|tr -d '\n') # For logging
     log "Simulation will start in ${delay}s"
     delayStart=$(date "+%s") # Used to compute the idle energy consumption
     sleep $delay
     simStart=$(date "+%s")
     echo "---------- Simulation (key=${simKey}) start at $simStart ($(date -d @${simStart}))" >> $logFile
     python ./clients.py $serverIp $nbSensors $nbSensorsFactor $requestPerSensor $sensorsSendInterval
     simEnd=$(date "+%s")
     echo "Simulation parameters: serverNode:$serverNode serverIp:$serverIp serverMac:$serverMac clientNode:$clientNode clientNode:$clientNode clientMac:$clientMac delay:$delay delayStart:${delayStart} nbSensors:$nbSensors nbSensorsFactor:$nbSensorsFactor requestPerSensors:$requestPerSensor sensorsSendInterval:${sensorsSendInterval} simKey:${simKey} simStart:${simStart} simEnd:${simEnd} duration:$(( simEnd - simStart )) serverNodeName:${serverNodeName} vmSize:${vmSize}" >> $logFile
     echo "./recordEnergy.sh nova $serverNodeName $simStart $simEnd energy_${simKey}_${nbSensors}NS_${vmSize}vmSize_${simStart}_${simEnd}.csv" >> $logFile
     echo -e "---------- Simulation (key=${simKey}) end at ${simEnd} ($(date -d @${simEnd}))\n" >> $logFile
     log "Simulation end ! Please see $logFile for more infos"
     ##### End Simulation #####

     ##### Print some infos #####
     log "Network Settings:"
     log " - Server $serverNode, $serverIp, $serverMac"
     log " - Client $clientNode, $clientIp, $clientMac"
     log "Simulation Settings:"
     log " - Simulation delay ${delay}s"
     log " - Number of sensors $(( nbSensors * nbSensorsFactor))"
     log " - Number of request per sensors $requestPerSensor"
     log " - Number of request per seconds on eachsensors $sensorsRequestPerSec"
     
elif [ "$1" = "kill" ] && [ $# -eq 1 ]
then
    ##### Kill all kvm on the subscribed nodes #####
    isServer=1
    finished=0
    for node in $(cat $OAR_NODE_FILE|uniq)
    do
	[ $isServer -eq 1 ] && { curMac=$serverMac; isServer=0; serverNode=$node; } || { curMac=$clientMac; finished=1; clientNode=$node; }
	log "Killing vm on node $node"
	oarsh $node pkill -9 qemu &
	[ $finished -eq 1 ] && break
    done
elif [ "$1" = "inspect" ] && [ $# -eq 2 ]
then
    ##### Show content of the database #####
    mysql --host="$2" -u user --password="mysql" experiment -e "SELECT * FROM temperature;"
elif [ "$1" = "flush" ] && [ $# -eq 2 ]
then
    ##### Flush content of the temperature table #####
    log "Cleaning database table..."
    mysql --host="$2" -u user --password="mysql" experiment -e "TRUNCATE TABLE temperature;"
else
    echo "Usage:"
    echo " - $0 subscribe"
    echo " - $0 deploy"
    echo " - $0 kill"
    echo " - $0 inspect <serverIP>"
    echo " - $0 flush <serverIP>"
fi

