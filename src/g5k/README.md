# G5K Simulation Scripts

- client.py is run by utils.sh
- setup-mysql.sql used by utils.sh to configure server database
- utils.sh contains functionnality to: 
	- Subscribe in G5K
	- Kill all VMs on the subscribed nodes
	- Inspect the database
	- Purge the database
- run-sim.sh use utils.sh to run all the simulations
- record-energy.sh fetch energy csv files from G5K
- energyFromLogs.sh use record-energy.sh to fetch energy data using run-sim.sh logs
