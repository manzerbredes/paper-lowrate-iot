#!/bin/bash

# Parameters
delay=60 # Delay before starting simulation (let CPU energy going down on the server)
nbSensors=20 # Number of sensors that will send request to de server
nbSensorsFactor=3 # nbSensors*nbSensorFactor
simulationTime=300 # Approximative
sensorsSendInterval=10 # Delay between sensors requests
# requestPerSensor dynamically computed inside init-nodes
vmSize=2048 # Number of alocated ram
simKey="NONE"

# Where script is located
simScript=$(dirname $(readlink -f "$0"))/utils.sh

# Build a function using the script
initNodes () {
    source "$simScript"
}


##### Test VM RAM #####
simKey="vmSize"
for vmSize in $(echo 1024 2048 4096)
do
    initNodes deploy
    initNodes kill # Kill all vms (do not forget :D)
done

vmSize=2048 # Reset vmSize

simKey="nbSensors"
##### Test number of sensors #####
for nbSensors in $(echo 20 100 300)
do
    initNodes deploy
    initNodes kill # Kill all vms
done

