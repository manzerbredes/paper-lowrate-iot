#!/usr/bin/python
from __future__ import division
import os,sys,random, time,datetime
from subprocess import Popen

# Check script argument
if len(sys.argv) != 6:
    print("Usage: "+sys.argv[0]+" <mysqlServerIp> <nbSensors> <nbSensorsFactor> <requestPerSensor> <sendInterval>")
    exit(1)

# Init script parameters
serverIp=sys.argv[1]
nbSensors=int(sys.argv[2])
nbSensorsFactor=int(sys.argv[3])
effectiveNbSensors=nbSensors*nbSensorsFactor
requestPerSensor=int(sys.argv[4])
sendInterval=int(sys.argv[5])
avgSiteTemp=list()
for i in range(0,effectiveNbSensors):
    avgSiteTemp.append(random.randint(-10,30))


def insert(sensorId, value):
    """ Send value of sensorId into the database """
    stamp=int(time.mktime(datetime.datetime.today().timetuple()))
    insertCMD = "mysql -u user --password=mysql --host="+serverIp+" experiment -e"
    insertCMD=insertCMD.split()
    insertCMD.append("INSERT INTO temperature (id,stamp,val) VALUES("+str(sensorId)+","+str(stamp)+","+str(value)+");")
    Popen(insertCMD) # Run command asynchronously

def send():
    """ Send temperature of each sensors into the database """
    for i in range(0,effectiveNbSensors):
        insert(i,random.gauss(avgSiteTemp[i], 3))
    
    
# Print infos
print("Launching clients with:")
print(" - Mysql Server IP {:>20}".format(serverIp))
print(" - Number of sensors {:>18}".format(effectiveNbSensors))
print(" - Number of request per sensor {:>7}".format(effectiveNbSensors))

# Send data
for i in range(0, requestPerSensor):
    send()
    time.sleep(sendInterval) # We assume send() take no time


